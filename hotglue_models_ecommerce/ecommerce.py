from datetime import date, datetime, timezone
from typing import List, Optional, Any, Dict, ClassVar
from typing_extensions import Literal
from unicodedata import category
from pydantic import BaseModel, validator


def convert_datetime_to_iso_8601(dt: datetime) -> str:
    return dt.isoformat(timespec="milliseconds")


def convert_date_str(dt: datetime.date) -> str:
    return dt.strftime("%Y-%m-%d")


class Location(BaseModel):
    schema_name: ClassVar[str] = "Locations"
    id: Optional[str]
    name: Optional[str]


class Category(BaseModel):
    schema_name: ClassVar[str] = "Categories"
    id: Optional[str]
    name: Optional[str]


class Supplier(BaseModel):
    id: Optional[str]
    name: Optional[str]


class Options(BaseModel):
    name: str
    value: str


class CustomField(BaseModel):
    name: Optional[str]
    label: Optional[str]
    value: Optional[str]


class Variant(BaseModel):
    id: Optional[str]
    sku: Optional[str]
    price: Optional[float]
    currency: Optional[str]
    sale_price: Optional[float]
    cost: Optional[float]
    available_quantity: Optional[int]
    options: Optional[List[Options]]
    image_urls: Optional[List[str]]
    barcode: Optional[str]
    width: Optional[str]
    length: Optional[str]
    depth: Optional[str]
    weight: Optional[str]
    mass_unit: Optional[str]
    distance_unit: Optional[str]
    description: Optional[str]
    virtual: Optional[bool]
    custom_fields: Optional[List[CustomField]]


class Product(BaseModel):
    schema_name: ClassVar[str] = "Products"
    id: Optional[str]
    name: Optional[str]
    cost: Optional[float]
    short_description: Optional[str]
    sku: Optional[str]
    description: Optional[str]
    variants: Optional[List[Variant]]
    options: Optional[List[str]]
    location: Optional[Location]
    category: Optional[Category]
    categories: Optional[List[Category]]
    suppliers: Optional[List[Supplier]]
    published: Optional[bool]
    virtual: Optional[bool]
    active: Optional[bool]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    image_urls: Optional[List[str]]
    tax_code: Optional[str]
    origin_country_code: Optional[str]
    custom_fields: Optional[List[CustomField]]
    slug: Optional[str]

    class Stream:
        name = "Products"

    @validator("updated_at", "created_at")
    @classmethod
    def validate_all_fields_one_by_one(cls, field_value):
        return field_value.astimezone(tz=timezone.utc)

    class Config:
        json_encoders = {datetime: convert_datetime_to_iso_8601, date: convert_date_str}


class OrderLineItemOption(BaseModel):
    type: Optional[str]
    id: Optional[str]
    value: Optional[str]


class OrderLineItemOptions(BaseModel):
    __root__: List[OrderLineItemOption]


class OrderLineItem(BaseModel):
    id: Optional[str]
    product_id: Optional[str]
    product_name: Optional[str]
    sku: Optional[str]
    quantity: Optional[float]
    unit_price: Optional[float]
    total_price: Optional[float]
    discount_amount: Optional[float]
    tax_code: Optional[str]
    tax_amount: Optional[float]
    notes: Optional[str]
    options: Optional[List[OrderLineItemOption]]
    custom_fields: Optional[List[CustomField]]

    class Stream:
        name = "OrderLineItems"


class Products(BaseModel):
    __root__: List[Product]


class OrderLineItems(BaseModel):
    __root__: List[OrderLineItem]


class Address(BaseModel):
    id: Optional[str]
    line1: Optional[str]
    line2: Optional[str]
    line3: Optional[str]
    city: Optional[str]
    state: Optional[str]
    postal_code: Optional[str]
    country: Optional[str]
    prefix: Optional[str]
    custom_fields: Optional[List[CustomField]]


class ShippingLines(BaseModel):
    id: Optional[str]
    code: Optional[str]
    source: Optional[str]
    carrier: Optional[str]
    subtotal: Optional[float]
    total_tax: Optional[float]
    total_price: Optional[float]
    discount_amount: Optional[float]
    custom_fields: Optional[List[CustomField]]


class SalesOrder(BaseModel):
    schema_name: ClassVar[str] = "Sales Orders"
    id: Optional[str]
    order_number: Optional[str]
    parent_order_id: Optional[str]
    customer_id: Optional[str]
    customer_name: Optional[str]
    customer_email: Optional[str]
    customer_company: Optional[str]
    phone: Optional[str]
    billing_name: Optional[str]
    billing_email: Optional[str]
    billing_company: Optional[str]
    billing_phone: Optional[str]
    line_items: Optional[List[OrderLineItem]]
    shipping_lines: Optional[List[ShippingLines]]
    billing_address: Optional[Address]
    shipping_company: Optional[str]
    shipping_phone: Optional[str]
    shipping_name: Optional[str]
    shipping_email: Optional[str]
    shipping_address: Optional[Address]
    fulfilled: Optional[bool]
    paid: Optional[bool]
    subtotal: Optional[float]
    total_price: Optional[float]
    total_discount: Optional[float]
    total_shipping: Optional[float]
    total_tax: Optional[float]
    carrier: Optional[str]
    tracking_number: Optional[str]
    tracking_url: Optional[str]
    delivery_status: Optional[str]
    status: Optional[str]
    currency: Optional[str]
    payment_method: Optional[str]
    requested_date: Optional[datetime]
    tags: Optional[List[Optional[str]]]
    transaction_date: Optional[datetime]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    order_notes: Optional[str]
    custom_fields: Optional[List[CustomField]]
    store_id: Optional[str]
    store_name: Optional[str]
    cart_id: Optional[str]

    class Stream:
        name = "SalesOrders"


class SalesOrders(BaseModel):
    __root__: List[SalesOrder]


class Customer(BaseModel):
    schema_name: ClassVar[str] = "Customers"
    id: Optional[str]
    first_name: Optional[str]
    last_name: Optional[str]
    email: Optional[str]
    phone: Optional[str]
    company: Optional[str]
    address: Optional[Address]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    custom_fields: Optional[List[CustomField]]

    class Stream:
        name = "Customers"


class Customers(BaseModel):
    __root__: List[Customer]


class OrderNote(BaseModel):
    schema_name: ClassVar[str] = "Order Notes"
    id: Optional[str]
    order_number: Optional[str]
    order_id: Optional[str]
    order_status: Optional[str]
    author_id: Optional[str]
    author_name: Optional[str]
    note: Optional[str]
    customer_note: Optional[bool]
    status: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    custom_fields: Optional[List[CustomField]]

    class Stream:
        name = "OrderNotes"


class OrderNotes(BaseModel):
    __root__: List[OrderNote]


class Store(BaseModel):
    schema_name: ClassVar[str] = "Store"
    id: Optional[str]
    url: Optional[str]
    store_name: Optional[str]
    default_currency: Optional[str]

    class Stream:
        name = "Store"

class Stores(BaseModel):
    __root__: List[Store]


class Cart(BaseModel):
    id: Optional[int]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    shipping_address: Optional[Address]
    billing_address: Optional[Address]
    customer_id: Optional[int]
    customer_email: Optional[str]
    customer_first_name: Optional[str]
    customer_last_name: Optional[str]
    store_id: Optional[int]
    store_name: Optional[str]
    currency: Optional[str]
    items: Optional[List[Product]]

    class Stream:
        name = "Cart"


class Carts(BaseModel):
    __root__: List[Cart]

    class Stream:
        name = "Carts"


class Transaction(BaseModel):
    id: Optional[str]
    type: Optional[str]
    sku: Optional[str]
    amount: Optional[float]
    date: Optional[datetime]
    status: Optional[str]
    method: Optional[str]
    parent_id: Optional[str]
    order_id: Optional[str]
    notes: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]


class RefundItem(BaseModel):
    id: Optional[str]
    name: Optional[str]
    sku: Optional[str]
    quantity: Optional[float]
    product_id: Optional[str]
    order_item_id: Optional[str]
    variant_id: Optional[str]
    unit_price: Optional[float]
    total_price: Optional[float]
    total_discount: Optional[float]
    total_tax: Optional[float]
    requires_shipping: Optional[bool]
    status: Optional[str]
    notes: Optional[str]
    restocking_fee: Optional[float]
    restock: Optional[bool]
    adjustment_fee: Optional[float]


class Refund(BaseModel):
    id: Optional[str]
    created_at: Optional[datetime]
    updated_at: Optional[datetime]
    order_id: Optional[str]
    invoice_id: Optional[str]
    user_id: Optional[str]
    store_id: Optional[str]
    reason: Optional[str]
    method: Optional[str]
    status: Optional[str]
    currency: Optional[str]
    notes: Optional[str]
    gateway: Optional[str]
    adjustment_fee_total: Optional[float]
    subtotal: Optional[float]
    total_tax: Optional[float]
    total_amount: Optional[float]
    items: Optional[List[RefundItem]]
    transactions: Optional[List[Transaction]]

    class Stream:
        name = "Refund"


class Refunds(BaseModel):
    __root__: List[Refund]

    class Stream:
        name = "Refunds"